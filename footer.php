<?php
/**
 * The template for displaying the contact footer.
 *
 * Contains the closing of the #contact section
 *
 * @package 3Ways
 */
?>

	</div><!-- #content -->
	<?php if(is_front_page()){ ?>
      <!--Contact US-->
    <div id="contact" class="container-fluid contact-wrapper primary-bg">
        <h1 class="main contact neutral"><span>CONTACT US</span></h1>
        <!--<div id="footer" class="container">-->
            <div class="row-fluid">
                <?php if(threeways_option('wp_google_map') != '') { ?>
                    <div class="map col-xs-12 col-sm-12 col-md-6 col-lg-6" style="min-height: 520px;">
                        <?php echo do_shortcode(threeways_option('wp_google_map')) ?>
                    </div>
                <?php } ?>
                <div class="contact col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?php if(threeways_option('contact_form') != '') {?>
                        <?php echo do_shortcode( threeways_option('contact_form') ); ?>
                <?php } ?>
                
                <?php if(threeways_option('email_footer') != '') {?>
                    <div class="email-link"><span>Email:</span> <?php echo nl2br(threeways_option('email_footer')); ?></div>
                <?php } ?>
                
                <!--Footer Social Bar-->
                <ul class="footer-utility">
                    <?php if(threeways_option('twitter') != '') {?>
				    <li><a href="<?php echo threeways_option('twitter'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/twitter-inverse.png"></a></li>
				    <?php } ?>
                    <?php if(threeways_option('instagram') != '') {?>
                    <li><a href="<?php echo threeways_option('instagram'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/instagram-inverse.png"></a></li>
                    <?php } ?>
                    <?php if(threeways_option('facebook') != '') {?>
                    <li><a href="<?php echo threeways_option('facebook'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/facebook-inverse.png"></a></li>
                    <?php } ?>
                </ul>
                <!--End Social Bar-->
                </div>
            <!--</div>-->
        </div>
    </div>
    <!--End Contact Us-->
    
    
    
    
    
    
		<?php 
			$color		= threeways_option('footer_blog_color'); 
			
			
			/** Style Container */
			$style = ( 
				! empty( $color )) ? 
					sprintf( '%s', $color) : '';
			$css = '';
			if ( ! empty( $style ) ) {			
				$css = 'style="'. $style .'" ';
			}
        ?>
	<?php } ?>
<!--</div> #page -->
<?php wp_footer(); ?>
</body>
</html>
