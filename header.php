<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package 3Ways
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8">
    <!-- Title
	================================================== -->
	<title><?php bloginfo('name'); ?><?php if(is_front_page()){ echo ' - ' .get_bloginfo('description');} else echo wp_title(); ?>
    </title>
    <!-- Title / End -->
    
    <!-- Meta
	================================================== -->
	<meta name="description" content="<?php echo threeways_option( 'meta_description' );?>">
    <meta name="keywords" content="<?php echo threeways_option( 'meta_keyword' ); ?>">
	<meta name="author" content="<?php echo threeways_option( 'meta_author' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Meta / End -->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo threeways_option('favicon', false, 'url'); ?>">
    <link rel="icon" type="image/png" href="<?php echo threeways_option('favicon', false, 'url'); ?>" />
	<link rel="apple-touch-icon" href="<?php echo threeways_option('touch_icon', false, 'url'); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo threeways_option('touch_icon_72', false, 'url'); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo threeways_option('touch_icon_144', false, 'url'); ?>">
    <!-- Favicons / End -->
    <script type="text/javascript">
        oldieishere=false;
        ieishere=false;
    </script>
    <!--[if IE]>
        <script type="text/javascript">
            ieishere=true;
        </script>
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script type="text/javascript">
            oldieishere=true;
        </script>

    <![endif]-->
    
    <noscript>
    	<style>
        	#portfolio_list div.item a div.hover {
				top: 0px;
				left: -100%;
				-webkit-transition: all 0.3s ease;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				-ms-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			#portfolio_list div.item a:hover div.hover{
				left: 0px;
			}
        </style>
    </noscript>
    
	<?php
    //loads comment reply JS on single posts and pages
    //if ( is_single()) wp_enqueue_script( 'comment-reply' ); 
    ?>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Preloading
    ======================================================================== -->
	<!--<div class="mask-color">
        <div id="preview-area">
            <div class="spinner">
              <div class="dot1"></div>
              <div class="dot2"></div>
            </div>
        </div>
    </div>-->
	<!-- Preloading / End -->
    <?php if( is_front_page() ){ ?>
    
    <!-- Header
    ======================================================================== -->
    <header id="header" class="navbar navbar-inverse navbar-fixed-top" id="top" role="banner">
    	<div class="container" >
            <!-- Logo
            ======================================================================== -->
            <a href="<?php echo home_url(); ?>" class="logo">
                <?php 
                    $top   = '' ;
                    $left  = '' ;
                    $width = '' ;
                    if( threeways_option('logo_top') != '' )$top    = 'top:'.threeways_option('logo_top').'px;' ;
                    if( threeways_option('logo_left') != '' )$left  = 'left:'.threeways_option('logo_left').'px;';
                    if( threeways_option('logo_width') != '' )$width = 'width:'.threeways_option('logo_width').'px;';
                    if( threeways_option('custom_logo', false, 'url') !== '' ){
                        echo '<div class="logo-wrapper"><img style="'.$width.'"  src="'. threeways_option('custom_logo', false, 'url') .'" alt="'.get_bloginfo( 'name' ).'" /></div>';
                    }else{
                ?>
                    <div class="logo-img"><span>LOGO</span></div>
                <?php } ?>
            </a>
            <!-- Logo / End -->
            
            <!--Responsive Menu-->
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar"
                aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!--End responsive menu-->
            
            <!--Navbar-->
            <nav id="bs-navbar" class="collapse navbar-collapse">
                <ul class="menu-utility">
                    <?php if(threeways_option('twitter') != '') {?>
				    <li><a href="<?php echo threeways_option('twitter'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/twitter.png"></a></li>
				    <?php } ?>
                    <?php if(threeways_option('instagram') != '') {?>
                    <li><a href="<?php echo threeways_option('instagram'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/instagram.png"></a></li>
                    <?php } ?>
                    <?php if(threeways_option('facebook') != '') {?>
                    <li><a href="<?php echo threeways_option('facebook'); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/facebook.png"></a></li>
                    <?php } ?>
                </ul>
                     <?php
                        wp_nav_menu(array( 
                            'container' => false,
                            'container_class' => 'menu',
                            'menu_class' => 'nav navbar-nav',
                            'menu_id'         => 'threeways-menu',
                            'theme_location' => 'main_nav',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'fallback_cb' => false,
                        ));      
                    ?>
            </nav> 
            <!-- Menu / End -->
        </div>
    </header>
	<!-- Header / End -->
    <!--Home-->
    <div id="home" class="container-fluid">
        <?php oe_main_slider(); ?>
    </div>
	<?php } ?>
