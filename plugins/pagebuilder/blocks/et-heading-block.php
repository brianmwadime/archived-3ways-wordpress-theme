<?php
/**
 * This file control heading title block
 *
 * @package    3Ways
 * @package    EngineThemes
 */

// HEADING TITLE BLOCK
if(!class_exists('OE_Title_Heading')) :

class OE_Title_Heading extends AQ_Block {

	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __( '3Ways Heading Title', '3ways'),
			'size' => 'span12',
		);
		
		//create the block
		parent::__construct('OE_Title_Heading', $block_options);
	}

 	function form($instance) {
		
		$defaults = array(
			'title' 		=> '',
            'class'         => 'white',
			'sub_title' 	=> '',
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
        
        $class_types = array(
			'neutral' => 'White',
			'portfolio'  => 'Pink',
			'contact' 	=> 'Gray',
		);
		
		global $include_animation ;
		?>
        <div class="sortable-body">
            <p class="description">
                <label for="<?php echo $this->get_field_id('class') ?>">
                    <?php _e( 'Class', '3ways');?>
                    <?php echo aq_field_select('class', $block_id, $class_types, $class) ?>
                </label>
            </p>
            <p class="description">
                <label for="<?php echo $this->get_field_id('title') ?>">
                    <?php _e( 'Title', '3ways');?>
                    <?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
                </label>
            </p>
            <p class="description">
                <label for="<?php echo $this->get_field_id('sub_title') ?>">
                    <?php _e( 'SubTitle', '3ways');?>
                    <?php echo aq_field_textarea('sub_title', $block_id, $sub_title, $size = 'full') ?>
            	</label>
            </p>  
        </div>
		<?php
	}

	function block($instance) {
		$bg_color = '';
		extract($instance);
		//if($duration != '' && $animation != '') $duration_effect = 'style="-webkit-animation-duration: '.$duration.'ms; -moz-animation-duration: '.$duration.'ms; -o-animation-duration: '.$duration.'ms;animation-duration: '.$duration.'ms; animation-delay:'.$delay.'ms; -webkit-animation-delay:'.$delay.'ms; -moz-animation-delay:'.$delay.'ms;-o-animation-delay:'.$delay.'ms;"';
		echo '<h1 class="main '.$class.'"><span class="'.$class.'">'.$title.'</span></h1>';
        if($sub_title != '') {
            echo '<div class="row-fluid">';
            echo '<div class="col-xs-12">';
            echo '<p class="wow">'.$sub_title. '</p>';
            echo '</div>';
            echo '</div>';
        }
	}
	
 	function before_block($instance) {
		extract($instance);
		echo '';		
	}

	function after_block($instance) {
 		extract($instance);
 		echo '<!-- END 3WAYS-HEADING-TITLE-BLOCK -->';
	}

}

aq_register_block('OE_Title_Heading');
endif;