<?php
/**
 * This file control blog block
 *
 * @package    3Ways
 * @package    EngineThemes
 */

// CLEAR BLOCK
if( ! class_exists( 'OE_Team_Block' ) ) :

class OE_Team_Block extends AQ_Block {

	function __construct() {
		$block_options = array(
			'name' => __( '3Ways Team', '3ways'),
			'size' => 'span12',
		);
		
		//create the block
		parent::__construct('OE_Team_Block', $block_options);
	}

 	function form($instance) {
		
		$defaults = array(
			'quantity' 					=> '-1',
			'margin_top' 				=> '10',
			'margin_bottom' 			=> '10',
            'team_title' 				=> 'TEAM',
			'duration' 					=> '900', 
			'animation' 				=> 'None', 
			'delay' 					=> '0', 
			'slide_speed' 				=> '800', 
			'pagination_speed' 			=> '800', 
			'auto_play' 				=> 'true', 
			'navigation' 				=> 'true', 
			'pagination' 				=> 'false', 
			'pagination_numbers' 		=> 'false', 
			'items' 					=> '4', 
			'items_desktop' 			=> '3', 
			'items_desktop_small' 		=> '3', 
			'items_tablet' 				=> '2', 
			'items_mobile' 				=> '1'
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		$auto_play_type = array(
			'true' 	=> 'True',
			'false' => 'False',
		);
		$navigation_type = array(
			'true' 	=> 'True',
			'false' => 'False',
		);
		$pagination_type = array(
			'true' 	=> 'True',
			'false' => 'False',
		);
		$pagination_numbers_type = array(
			'true' 	=> 'True',
			'false' => 'False',
		);
		
		global $include_animation ;
		?>
        <p class="description">
			<label for="<?php echo $this->get_field_id('team_title') ?>">
				Team section sub heading &nbsp;&nbsp;
				<?php echo aq_field_input('team_title', $block_id, $team_title) ?>
			</label>
		</p>
        <p class="description">
            <label for="<?php echo $this->get_field_id('margin_top') ?>">
                Margin top 
                <?php echo aq_field_input('margin_top', $block_id, $margin_top, 'min', 'number') ?> px
            </label>&nbsp;-&nbsp;
            <label for="<?php echo $this->get_field_id('margin_bottom') ?>">
                Margin bottom
                <?php echo aq_field_input('margin_bottom', $block_id, $margin_bottom, 'min', 'number') ?> px
            </label>
        </p>
        <p class="description">
			<label for="<?php echo $this->get_field_id('quantity') ?>">
				Quantity ( * Note : with the value '-1', all item will be displayed ! )&nbsp;&nbsp;
				<?php echo aq_field_input('quantity', $block_id, $quantity, 'min', 'number') ?>
			</label>
		</p> 
        <p class="description">
            <label for="<?php echo $this->get_field_id('navigation') ?>">
                Display "next" and "prev" buttons.<br/>
                <?php echo aq_field_select('navigation', $block_id, $navigation_type , $navigation) ?>
            </label>
        </p>
        <p class="description">
			<label for="<?php echo $this->get_field_id('items') ?>">
				Items ( This variable allows you to set the maximum amount of items displayed at a time with the widest browser width )<br/>
				<?php echo aq_field_input('items', $block_id, $items, 'min', 'number') ?>
			</label>
		</p>

		<?php
	}

	function block($instance) {
		extract($instance);
		$query = new WP_Query(array(
			'post_type' 	 => 'team',
			'posts_per_page' => $quantity
		));
		$i = 0;
		global $post;
		if($query->have_posts()){
			while($query->have_posts()){
				$query->the_post();
                $mytitle = get_the_title();
                $name = explode(" ", $mytitle);
		?>
			<div class="col-xs-12 col-sm-6 col-md-4">
                <?php the_post_thumbnail( 'teams', array('class' => 'center-block img-thumbnail img-circle') ); ?>
                <a href="<?php echo get_post_meta( $post->ID, 'oe_team_tw', true ); ?>" class="team-link">
                    <span><?php echo $name[0];  ?></span>
                    <?php echo $name[1]; ?>
                    <img src="<?php echo get_template_directory_uri() ?>/images/twitter-inverse.png"/>
                </a>
			</div>
		<?php
			$i++;
			}
		}
		wp_reset_query();
	}
 	function before_block($instance) {
		extract($instance);
        echo '<div class="clearfix"></div>';
		echo 	'<div class="row-fluid">';
        echo    '<h2 class="sub-main">'.$team_title.'</h1>';
	}

	function after_block($instance) {
 		extract($instance);
 		echo  '</div>';
	}

}

aq_register_block( 'OE_Team_Block' );
endif;