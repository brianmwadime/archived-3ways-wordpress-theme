#3Ways - Multi-purpose One Page WordPress Theme

[Screenshot](screenshot.png)
<img src="screenshot.png" alt="Screenshot" width="872">

## Installation
* **Clone** the repository into the wordpress themes section.
* or **download** an archive of the project and extract it into the wordpress themes section.

## Changelog

See [CHANGELOG.md](CHANGELOG.md).

Copyright (C) 2016 - 3Ways (3ways.co.ke) 
Developed by Fortune Kidew Consulting (fortunekidew.co.ke)